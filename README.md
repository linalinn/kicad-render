# KiCad render
This Repo contains a job template if you want to integrate the rendering of your PCB into an existing pipeline or a complete pipeline with just a few configurations needed Based on the Image I made for the [GitHub Action](https://github.com/linalinn/kicad-render)
## Configuration options

```yaml
variables:
  PCB_FILE: "my cool board.kicad_pcb"
  OUTPUT_PATH: public
  ANIMATION: "gif"
```

`PCB_FILE` Path to kicad pcb file  
`OUTPUT_PATH` Path where the resulting images are stored  
`ANIMATION` set to gif to create an animation of your pcb rotating


## Includeing the Pipeline
 1. create a `.gitlab-ci.yml` in the root of your repository
 2. Add the following snippet to the `.gitlab-ci.yml`
    ```yaml
    include:
      - remote: 'https://gitlab.com/linalinn/kicad-render/-/raw/main/kicad-render-pipeline.yml'

    pages:
      variables:
        PCB_FILE: "$CI_PROJECT_DIR/test pcb/test.kicad_pcb"
    ```
3. Change the value of `PCB_FILE` to the path of your `.kicad_pcb` file from the root of your project it is a good practice to add the variable `$CI_PROJECT_DIR` in so `$CI_PROJECT_DIR/<path from repo root to .kicad_pcb>`


4. (optional) Add `ANIMATION: gif` below or above `PCB_FILE` to render an animation of your PCB rotating
5. Adding the images to an README.md
    ```Markdown
    # My first PCB with automatic image generation

    ### Images
    ![top](https://<gitlab_username>.gitlab.io/<repo_name>/top.png)
    ![bottom](https://<gitlab_username>.gitlab.io/<repo_name>/bottom.png)
    ```
6. (optional) Adding the Animation (**requires step 4**)
    ```Markdown
    ### Animation
    ![animation](https://<gitlab_username>.gitlab.io/<repo_name>/rotating.gif)
    ```
7. git commit and push after the pipeline is finished your images should be visible.


## Including a job template into an existing Pipeline

1. Add the following `include` to you `.gitlab-ci.yml`
    ```yaml
    include:
      - remote: 'https://gitlab.com/linalinn/kicad-render/-/raw/main/kicad-render-job-template.yml'
    ```

2. Create your job and extend it with `.render_images` see the example below
    ```yaml
    my-job:
      stage: my-stage
      extends: .render_images
      variables:
        OUTPUT_PATH: public
        PCB_FILE: "<path to .kicad_pcb>"
      artifacts:
        paths:
          - public
    ```